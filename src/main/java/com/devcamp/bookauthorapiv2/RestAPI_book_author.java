package com.devcamp.bookauthorapiv2;


import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin   //Java @CrossOrigin: cho phép CORS trên RESTful web service.
@RestController
public class RestAPI_book_author {  

    //http://localhost:8080/books
    @GetMapping("/books")
    public ArrayList<Book> splitString() {

        // khởi tạo 6 đối tượng tác giả 
        Author tac_gia1 = new Author("thuong", "thuong@gamil.com", 'f'); // 'm'  of 'f'
        Author tac_gia2 = new Author("linh", "linh@gamil.com", 'f');

        Author tac_gia3 = new Author("duong", "duong@gamil.com", 'm');
        Author tac_gia4 = new Author("thinh", "thinh@gamil.com", 'f'); // 'm'  of 'f'

        Author tac_gia5 = new Author("trung", "trung@gamil.com", 'f'); // 'm'  of 'f'
        Author tac_gia6 = new Author("huy", "huy@gamil.com", 'f'); // 'm'  of 'f'

        // khởi tạo 3 đối tượng Arrlist ( mỗi đối tượng thêm 2 tác giả )
        ArrayList<Author> authorlist1 = new ArrayList<Author>();
        ArrayList<Author> authorlist2 = new ArrayList<Author>();
        ArrayList<Author> authorlist3 = new ArrayList<Author>();
        // thêm tác giả vào Arrlist
        authorlist1.add(tac_gia1);
        authorlist1.add(tac_gia2);
        
        authorlist2.add(tac_gia3);
        authorlist2.add(tac_gia4);

        authorlist3.add(tac_gia5);
        authorlist3.add(tac_gia6);



        // khởi tạo 3 đối tượng sách 
        Book book1 = new Book("toan",authorlist1, 1000);
        Book book2 = new Book("ly",authorlist2, 2000);
        Book book3 = new Book("hoa",authorlist3, 3000);
        //Sử dụng phương thức toString để in 3 đối tượng trên ra console
        System.out.println(book1);
        System.out.println(book1);
        System.out.println(book1);
       
        ArrayList<Book> listString = new ArrayList<Book>();
        // thêm sách vào arr list
        listString.add(book1);
        listString.add(book2);
        listString.add(book3);
        
        // trả về là arr list
        return listString;
    }

    
}
