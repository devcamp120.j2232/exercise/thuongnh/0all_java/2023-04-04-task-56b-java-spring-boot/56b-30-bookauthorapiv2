package com.devcamp.bookauthorapiv2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookAuthorApiv2Application {

	public static void main(String[] args) {
		SpringApplication.run(BookAuthorApiv2Application.class, args);
	}

}
